import * as R from 'ramda';
import apisauce from 'apisauce';

import { YOUTUBE_API } from '~/constants/api';

const ErrorKind = {
  UNKNOWN_ERROR: 'UNKNOWN_ERROR',
  NETWORK_ERROR: 'NETWORK_ERROR',
  SERVER_ERROR: 'SERVER_ERROR',
  CLIENT_ERROR: 'CLIENT_ERROR',
  API_ERROR: 'API_ERROR',
  DATA_ERROR: 'DATA_ERROR',
  WRONG_REPONSE_FORMAT: 'WRONG_REPONSE_FORMAT',
};

const ApiError = (kind, message = '', code = '') => ({
  kind,
  message: message || kind,
  code: String(code),
});

export default class BaseApi {
  static ErrorKind = ErrorKind;

  options = {
    baseURL: YOUTUBE_API,
    headers: {
      'Cache-Control': 'no-cache',
    },
    timeout: 20000,
  };

  static ApiError = ApiError;

  constructor(options) {
    this.options = { ...this.options, ...options };
    this.api = apisauce.create({
      baseURL: this.options.baseURL,
      headers: this.options.headers,
      timeout: this.options.timeout,
      // paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'repeat' }),
    });
    if (process.env.NODE_ENV === 'development' && process.env.BUILD_TARGET === 'web') {
      this.api.addMonitor(this.clientMonitor);
    }
  }

  clientMonitor = response => {
    console.log('====================================');
    console.log(`API MONITOR: ${response.config.method.toUpperCase()} ${response.config.url}`, {
      params: response.config.params,
      data: response.data,
      response,
    });
    console.log('====================================');
  };

  checkForErrors = (kind, response) => {
    if (response.data && typeof response.data === 'object') {
      const { error } = response.data;
      if (error) {
        throw new ApiError(ErrorKind.API_ERROR, error);
      }
      return;
    }

    if (!response.ok) {
      const { problem } = response;
      if (problem === 'CLIENT_ERROR') {
        throw new ApiError(
          ErrorKind.CLIENT_ERROR,
          `Client error: ${response.status}`,
          response.status,
        );
      } else if (problem === 'SERVER_ERROR') {
        throw new ApiError(
          ErrorKind.SERVER_ERROR,
          `Server error: ${response.status}`,
          response.status,
        );
      } else if (
        R.contains(problem, ['TIMEOUT_ERROR', 'CONNECTION_ERROR', 'NETWORK_ERROR', 'CANCEL_ERROR'])
      ) {
        throw new ApiError(ErrorKind.NETWORK_ERROR, problem);
      }
      throw new ApiError(ErrorKind.UNKNOWN_ERROR, problem);
    }
    throw new ApiError(ErrorKind.WRONG_REPONSE_FORMAT, JSON.stringify(response.data));
  };

  get = async (entity = '', params = {}) => {
    const response = await this.api.get(entity, {
      ...params,
    });

    this.checkForErrors('get', response);
    return response.data || {};
  };
}
