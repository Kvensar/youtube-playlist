/* eslint-disable import/prefer-default-export */
import JSPath from 'jspath';

export function normalizeArrayOfEntities(array, { idField = 'id', entityConvertor } = {}) {
  const ids = [];
  const entityById = {};
  for (let i = 0; i < array.length; i += 1) {
    const entity = entityConvertor ? entityConvertor(array[i]) : array[i];
    ids.push(entity[idField]);
    entityById[entity[idField]] = entity;
  }
  return { ids, entityById };
}

export function convertPlaylistEntity(videoItem) {
  return {
    id: JSPath.apply('..snippet.resourceId.videoId[0]', videoItem),
    title: JSPath.apply('..snippet.title[0]', videoItem),
    thumbnail: JSPath.apply('..snippet.thumbnails.high.url[0]', videoItem),
  };
}
