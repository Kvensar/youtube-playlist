import * as R from 'ramda';

import { createReducer, createActions } from 'reduxsauce';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions(
  {
    fetchPlaylistStarted: [],
    fetchPlaylistSuccess: ['ids', 'entityById'],
    fetchPlaylistError: ['errorObj'],
  },
  { prefix: 'PLAYLIST/' },
);

export const ActionTypes = Types;

export default Creators;

/* ------------- Initial State ------------- */

// type EventSeriesState = {
//   videos: Array,
// status: string
// };

export const INITIAL_STATE = {
  videoIds: [],
  videoData: {},
  status: '',
};

/* ------------- Reducer functions ------------- */

const fetchPlaylistStarted = state => {
  return {
    ...state,
    status: 'inProgress',
  };
};

const fetchPlaylistSuccess = (state, { ids, entityById }) => {
  // const classes = page === 1 ? data : R.concat(state.classes, data);
  return {
    ...state,
    videoIds: ids,
    videoData: entityById,
    status: 'success',
  };
};

const fetchPlaylistError = (state, { errorObj }) => {
  return {
    ...state,
    videos: [],
    status: errorObj,
  };
};

/* ------------- Hook  ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [ActionTypes.FETCH_PLAYLIST_STARTED]: fetchPlaylistStarted,
  [ActionTypes.FETCH_PLAYLIST_SUCCESS]: fetchPlaylistSuccess,
  [ActionTypes.FETCH_PLAYLIST_ERROR]: fetchPlaylistError,
});
