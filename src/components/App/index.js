import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Container, Segment, Header } from 'semantic-ui-react';
import YouTube from 'react-youtube';

import Playlist from '~/components/Playlist';
import Api from '~/api/playlist';

import './styles.scss';

const api = new Api();

const mapStateToProps = state => ({
  videoIds: state.playlist.videoIds,
  videoData: state.playlist.videoData,
  status: state.playlist.status,
});

const mapDispatchToProps = dispatch => ({
  fetchPlaylist: () => {
    dispatch(api.fetchPlaylist());
  },
});

export default
@connect(
  mapStateToProps,
  mapDispatchToProps,
)
class App extends PureComponent {
  state = {
    activeVideoIndex: 0,
    activeVideoId: '',
  };

  playerRef = React.createRef();

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      prevState.activeVideoId !== nextProps.location.hash.replace('#', '') &&
      nextProps.videoIds.length
    ) {
      const videoId = nextProps.location.hash && nextProps.location.hash.replace('#', '');
      const index = nextProps.videoIds.indexOf(videoId);
      if (index !== -1) return { activeVideoIndex: index, activeVideoId: videoId };
    }
    return null;
  }

  componentDidMount() {
    this.props.fetchPlaylist();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.hash !== this.props.location.hash && this.playerRef) {
      this.playerRef.current.internalPlayer.playVideo();
    }
  }

  setActiveVideoIndex = newIndex => {
    this.props.history.push(`/#${this.props.videoIds[newIndex]}`);

    this.setState({
      activeVideoIndex: newIndex,
    });
  };

  render() {
    const {
      videoIds,
      videoData,
      location: { path, hash },
    } = this.props;
    const { activeVideoIndex } = this.state;

    const activeVideoId = hash && hash.replace('#', '');

    return (
      <Container>
        <Segment>
          <Header as="h2" textAlign="center">
            {(videoData[activeVideoId] && videoData[activeVideoId].title) || 'Select a video'}
          </Header>
        </Segment>

        <YouTube videoId={activeVideoId} ref={this.playerRef} />

        <Segment>
          <Playlist
            videoData={videoData}
            videoIds={videoIds}
            activeVideoId={activeVideoId}
            activeVideoIndex={activeVideoIndex}
            setActiveVideoIndex={this.setActiveVideoIndex}
            path={path}
          />
        </Segment>
      </Container>
    );
  }
}

App.propTypes = {
  history: PropTypes.instanceOf(PropTypes.object),
  location: PropTypes.instanceOf(PropTypes.object),

  videoData: PropTypes.instanceOf(PropTypes.object),
  videoIds: PropTypes.arrayOf(PropTypes.string),
  fetchPlaylist: PropTypes.func.isRequired,
};
