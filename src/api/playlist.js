import BaseApi from './BaseApi';

import { API_KEY } from '../constants/api';

import Actions from '../reducers/playlist';
import { normalizeArrayOfEntities, convertPlaylistEntity } from '../utils/normalizeData';

export default class Api {
  constructor() {
    this.api = new BaseApi();
  }

  fetchPlaylist = () => {
    return async dispatch => {
      dispatch(Actions.fetchPlaylistStarted());
      try {
        const data = await this.api.get('playlistItems', {
          part: 'snippet',
          playlistId: 'PLPxbbTqCLbGE5AihOSExAa4wUM-P42EIJ',
          key: API_KEY,
        });

        const { ids, entityById } = normalizeArrayOfEntities(data.items, {
          entityConvertor: convertPlaylistEntity,
        });

        dispatch(Actions.fetchPlaylistSuccess(ids, entityById));
      } catch (e) {
        if (process.env.NODE_ENV === 'development') {
          console.error('GET: Playlist', e);
        }
        dispatch(Actions.fetchPlaylistError(e));
      }
    };
  };
}
