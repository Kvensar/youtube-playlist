import { combineReducers } from 'redux';

import { reducer as playlist } from './playlist';

export default combineReducers({
  playlist,
});
