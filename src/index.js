import React from 'react';
import { Provider } from 'react-redux';
import { hydrate } from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';

import createStore from './createStore';
import App from './components/App';

const root = document.getElementById('root');

const store = createStore();

const jsx = (
  <Provider store={store}>
    <BrowserRouter>
      <Route component={App} />
    </BrowserRouter>
  </Provider>
);

hydrate(jsx, root);

if (module.hot) {
  // Enable Webpack hot module replacement for components
  module.hot.accept();

  // Enable Webpack hot module replacement for reducers
  // module.hot.accept('./reducers', () => {
  //   const nextRootReducer = require('./reducers').default;
  //   store.replaceReducer(nextRootReducer);
  // });
}
